""" Class design for Dataset"""

import json
import os
import copy
import csvdiff
import pandas as pd
from IPython.display import display

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    FileNotFoundException, SerializationFailedException, \
    DeserializationFailedException, \
    InvalidDatatypeException
from xpresso.ai.core.commons.utils.constants import DATA_DIFFERENCE_FILENAME, \
    DATASET_DIFFERENCE_PATH, METADATA_DIFFERENCE_FILENAME, NGRAMS, CORRELATIONS
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.data.automl import utils
from xpresso.ai.core.data.automl.attribute_info import AttributeInfo
from xpresso.ai.core.data.automl.data_type import DataType
from xpresso.ai.core.data.automl.abstract_dataset import AbstractDataset, \
    DatasetEncoder, DatasetDecoder
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.connections import Connector
from xpresso.ai.core.data.automl.dataset_info import DatasetInfo

from xpresso.ai.core.data.automl.abstract_dataset import AbstractDataset
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.connections import Connector
from xpresso.ai.core.data.automl.data_type import DataType
from xpresso.ai.core.data.automl.dataset_info import DatasetInfo
from xpresso.ai.core.commons.utils.constants import DATA_DIFFERENCE_FILENAME, \
    DATASET_DIFFERENCE_PATH, METADATA_DIFFERENCE_FILENAME
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ['StructuredDataset']
__author__ = 'Srijan Sharma'

# This is indented as logger can not be serialized and can not be part
# of automl
LOGGER = XprLogger()


class StructuredDataset(AbstractDataset):
    """ StructuredDataset stores the data in tabular format. It reads data
    from csv, excel or any database. It stores the automl into local storage
    in pickle format."""

    def __init__(self, dataset_name: str = "default_dataset",
                 project_name: str = "default_project",
                 created_by: str = "default",
                 description: str = "This is a structured automl"):
        super().__init__(dataset_name=dataset_name,
                         description=description,
                         project_name=project_name,
                         created_by=created_by)

        self.info = DatasetInfo()
        self.type = DatasetType.STRUCTURED

    def import_dataset(self, user_config, local_storage_required: bool = False,
                       sample_percentage: float = 100):

        """ Fetches automl from multiple data sources and loads them
        into a automl"""

        kwargs = user_config.get("options")

        if type(kwargs) is not dict:
            LOGGER.error(
                        "Unsupported keyword argument supplied for data "
                        "connector.")
            kwargs = dict()

        self.data = Connector().getconnector(
                    user_datasource=user_config.get("type"),
                    datasource_type=user_config.get(
                                "data_source")).import_dataframe(
                    user_config, **(kwargs if kwargs is not None else {}))

        self.local_storage_required = local_storage_required
        self.sample_percentage = sample_percentage

    def save(self):
        """ Save the data into the local file system in
        a serialized format

        Returns:
            str: json file path where serialized metadata, metrics has been
            stored
            str: data_file_path where csv data has been stored
        """
        data_file_path = self.get_csv_file_path()
        self.data.to_csv(data_file_path, index=False)
        json_file_path = self.json_serialize()
        if not os.path.exists(json_file_path) or not os.path.exists(
                    data_file_path):
            LOGGER.error("Unable to save {} dataset".format(self.name))
        folder_path = os.path.dirname(data_file_path)
        return folder_path

    def load(self, directory_path):
        """
        Load the data set from local storage and deserialize to update
        the dataset
        Args:
            directory_path(str): path where json file (i.e. metrics,
            metadata) and csv file (data) is stored
        """
        json_data_path = utils.get_json_from_dir(directory_path)
        data_file_path = utils.get_csv_from_dir(directory_path)
        try:
            self.json_deserialize(json_data_path)
            self.data = pd.read_csv(data_file_path)
        except FileNotFoundError:
            raise FileNotFoundException

    def diff(self, new_dataset, output_path=DATASET_DIFFERENCE_PATH):
        """ Finds the difference between two automl class"""
        if not isinstance(new_dataset, StructuredDataset):
            LOGGER.error("Unacceptable Data type provided for new.")
            raise InvalidDatatypeException("Dataset diff not supported for "
                                           "given new dataset")

        metadata_diff = self.compare_metadata(new_dataset)
        data_diff = self.compare_data(new_dataset)

        self.output_diff(new_dataset, data_diff, metadata_diff, output_path)

    def output_diff(self, new, data_diff, metadata_diff, output_path):
        """helper function output data difference between automl to excel file
        Args:
            data_diff('list'): Rows added, removed and changed in new
                automl
            metadata_diff('list'): list of tuple of the difference in metadata
            of two datasets
            new(StructuredDataset obj): New structured dataset object
            output_path(str): Output for the difference excel file to be
            stored in.
            """
        sheets = dict()
        if metadata_diff:
            metadata_data_frame = list()
            metadata_data_frame.append(["Atrribute", "Modification_type"])
            for data in metadata_diff:
                metadata_data_frame.append([data[0], data[1]])
            data_frame = pd.DataFrame(metadata_data_frame)
            sheets["metadata_diff"] = data_frame
        old = self
        data_diff_filename = utils.append_timestamp(DATA_DIFFERENCE_FILENAME)
        modification_type = ["added", "removed"]
        for ittr in modification_type:
            rows_data = list()
            rows_data.append(["Old Dataset: {}".format(old.name)])
            rows_data.append(["New Dataset: {}".format(new.name)])
            rows_data.append([])
            rows_data.append([])

            diff_data = data_diff[ittr]
            if diff_data:
                rows_data.append(list(diff_data[0].keys()))
                for data in diff_data:
                    rows_data.append(list(data.values()))
                rows_data = pd.DataFrame(rows_data)
            data_df = pd.DataFrame(rows_data)
            sheets[ittr] = data_df
        utils.to_excel(output_path, data_diff_filename, sheets)

    def compare_metadata(self, new_dataset):
        """
        Compares the metadata of two automl classes i.e. attributeInfo for
        each automl is compared
        """
        old = self.info.attributeInfo
        new = new_dataset.info.attributeInfo
        identical = True
        metadata_old = list()
        metadata_new = list()
        difference = list()

        for attr in new:
            metadata_new.append((attr.name, attr.dtype, attr.type))

        for attr in old:
            metadata_old.append((attr.name, attr.dtype, attr.type))

        metadata_diff = list(set(metadata_old).symmetric_difference(set(
                    metadata_new)))

        for attr_diff in metadata_diff:

            if attr_diff in metadata_old:
                name = attr_diff[0]
                attr_type = attr_diff[2]

                # If the name is present in the old attributeinfo, but not in
                # the new one
                if name not in [attr[0] for attr in metadata_new]:
                    print("{} has been removed.Not found in the latest "
                          "version".format(attr_diff))
                    difference.append((name, "removed"))
                    identical = False

                # if the attribute corresponding to that name is present in
                # old and new attributeinfo, but only the type has changed
                for attr_new in metadata_new:
                    if name is attr_new[0] and attr_type is not attr_new[2]:
                        print("Type of {} changed from {} to {} in the "
                              "latest version".format(name, attr_type,
                                                      attr_new[2]))
                        difference.append((name, "updated"))
                        identical = False
                        break

            # if the attribute is present in latest version but not in the old
            # version
            elif attr_diff in metadata_new:
                name = attr_diff[0]
                print("{} added in the latest version".format(attr_diff))
                difference.append((name, "added"))
                identical = False

        if identical:
            print("Metadata for both versions identical")
        return difference

    def compare_data(self, new_dataset):
        """
        Compares the pandas dataframe of two automl classes
        """
        new = copy.copy(new_dataset.data)
        old = copy.copy(self.data)
        common_columns = list(set(new.columns) & set(
                    old.columns))
        new = new[common_columns]
        old = old[common_columns]
        new['id'] = new.apply(lambda x: hash(tuple(x)), axis=1)
        old['id'] = old.apply(lambda x: hash(tuple(x)), axis=1)
        old_records = old.to_dict("records")
        new_records = new.to_dict("records")
        data_diff = csvdiff.diff_records(old_records, new_records, ['id'])
        return data_diff

    def unique(self, attr_name=None, top=None):
        """Function to get top n unique categories
        of a categorical attribute
        Args:
            attr_name('str'): Attribute name of which unique category to be
            listed
            top('int'): Number of top categories to be listed"""
        if attr_name is None:
            print("provide attribute name")
            return
        for attr in self.info.attributeInfo:
            if attr.name != attr_name:
                continue
            if attr.type not in [DataType.NOMINAL.value,
                                 DataType.ORDINAL.value]:
                print("{} is not a categorical attribute".format(attr_name))
                return
            unique = attr.metrics["freq_count"]
            unique = pd.Series(unique).to_frame()
            if top is not None:
                unique = unique.head(top)
            display(unique)

    def filter(self, items=None, like=None, axis=None, regex=None):
        """Filters the automl into subset
        items('list'): Keep labels from axis which are in items.
        like('string'): Keep labels from axis for which “like in label == True”.
        regex('string'): Keep labels from axis for which re.search(regex,
        label) == True.
        axis('int'): The axis to filter on. By default
        this is ‘columns’ for DataFrame.
        """
        self.data = self.data.filter(items=items, like=like, axis=axis,
                                     regex=regex)

    def change_type(self, attribute_name, new_type):
        """Change the type of an attribute
                Args:
                    attribute_name('str'): attribute whose type is to be changed
                    new_type('str'): New type t assigned to the attribute"""
        attr_list = list(filter(lambda attr: attr.name == attribute_name,
                                self.info.attributeInfo))
        if not attr_list:
            # print is required,  to display on the message on the user side
            print("{} attribute doesn't exist".format(attribute_name))
            return
        attr = attr_list[0]
        previous_type = attr.type
        if new_type == DataType.NUMERIC.value and attr.type == \
                    DataType.NOMINAL.value and \
                    (attr.dtype == DataType.FLOAT.value
                     or attr.dtype == DataType.INT.value):
            attr.type = new_type
        elif new_type == DataType.NUMERIC.value and attr.type == \
                    DataType.STRING.value:
            attr.type = new_type
        elif new_type == DataType.NOMINAL.value and (
                    attr.type == DataType.NUMERIC.value or
                    attr.type == DataType.TEXT.value):
            attr.type = new_type
        elif new_type == DataType.TEXT.value and (
                    attr.type in [DataType.ORDINAL.value,
                                  DataType.NOMINAL.value,
                                  DataType.NUMERIC.value, DataType.STRING.value,
                                  DataType.TEXT.value, DataType.DATE.value]):
            attr.type = new_type

        if attr.type != new_type:
            # print is required,  to display on the message on the user side
            print("{} to {} not possible for {} attribute".format(attr.type,
                                                                  new_type,
                                                                  attr.name))
            return
        # print is required,  to display on the message on the user side
        print("Datatype of {} changed from {} to {}.".format(attr.name,
                                                             previous_type,
                                                             new_type))
        if new_type == DataType.NUMERIC.value:
            self.data[attr.name] = pd.to_numeric(self.data[attr.name],
                                                 errors="coerce")

    def json_serialize(self):
        """ Helper function to serialize structured dataset metadata and
                metrics"""
        temp_dataset = self.copy_metadata()
        for attr in temp_dataset.info.attributeInfo:
            try:
                del attr.logger
            except AttributeError:
                LOGGER.warning("AttributeInfo not present")

        for attr in temp_dataset.info.attributeInfo:
            if attr.type == "text" and attr.metrics:
                attr.metrics = self.metric_serialize_deserialize(attr.metrics,
                                                                 serialize=True,
                                                                 keys=NGRAMS)

        if temp_dataset.info.metrics:
            temp_dataset.info.metrics = self.metric_serialize_deserialize(
                        temp_dataset.info.metrics, serialize=True,
                        keys=CORRELATIONS)

        try:
            json_file_path = self.get_json_file_path()
            with open(json_file_path, 'w', encoding='utf-8') as file:
                json.dump(temp_dataset, file, ensure_ascii=False, indent=4,
                          cls=DatasetEncoder)
        except (TypeError, OverflowError, ValueError):
            raise SerializationFailedException("JSON serialization failed")
        return json_file_path

    def show(self, k=5):
        """Displays first and last k rows of the dataframe
        Args:
            k (int): Number of rows to be shown
        """
        if not self.data:
            # print is required,  to display on the message on the user side
            print("Unable to display empty data frame")
        print("Top {} rows".format(k))
        display(self.data.head(k))
        print("\n Last {} rows".format(k))
        display(self.data.tail(k))

    def json_deserialize(self, json_data_path):
        """
        Extends helper function to deserialize json object to
        structured dataset object
        Args:
            json_data_path(str): json file path to load"""
        super().json_deserialize(json_data_path)
        try:
            with open(json_data_path) as json_file:
                json_data = json.load(json_file, cls=DatasetDecoder)
        except (json.JSONDecodeError, TypeError):
            raise DeserializationFailedException("JSON deserialization failed")

        # before understand
        for key, value in json_data.items():
            setattr(self, key, value)

        try:
            self.config = XprConfigParser()
        except KeyError:
            LOGGER.warning("'Config' key error in json data ")

        try:
            info = json_data["info"]
            self.info = DatasetInfo()
            for key, value in info.items():
                setattr(self.info, key, value)
        except KeyError:
            LOGGER.warning("Info key error in json data")
        try:
            attribute_info = json_data["info"]["attributeInfo"]
            self.info.attributeInfo = list()
            for attr in attribute_info:
                var = AttributeInfo(attr["name"])
                for key, value in attr.items():
                    setattr(var, key, value)
                self.info.attributeInfo.append(var)
        except KeyError:
            LOGGER.warning("Info key error in json data")

        for attr in self.info.attributeInfo:
            if attr.type == "text" and attr.metrics:
                attr.metrics = self.metric_serialize_deserialize(attr.metrics,
                                                                 serialize=False,
                                                                 keys=NGRAMS)
        if self.info.metrics:
            self.info.metrics = self.metric_serialize_deserialize(
                        self.info.metrics,
                        serialize=False,
                        keys=CORRELATIONS)
