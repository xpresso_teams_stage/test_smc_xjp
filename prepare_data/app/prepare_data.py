import os
import sys
import dill
import pickle
import urllib
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    housing = pickle.load(open(f"{PICKLE_PATH}/housing.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    strat_train_set = pickle.load(open(f"{PICKLE_PATH}/strat_train_set.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    strat_test_set = pickle.load(open(f"{PICKLE_PATH}/strat_test_set.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    strat_train_x = pickle.load(open(f"{PICKLE_PATH}/strat_train_x.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    housing_labels = pickle.load(open(f"{PICKLE_PATH}/housing_labels.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    housing_num = pickle.load(open(f"{PICKLE_PATH}/housing_num.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    housing_cat = pickle.load(open(f"{PICKLE_PATH}/housing_cat.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    housing_prepared = pickle.load(open(f"{PICKLE_PATH}/housing_prepared.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = prepare_data
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["housing", "strat_train_set", "strat_test_set", "strat_train_x", "housing_labels", "housing_num", "housing_cat", "housing_prepared"]


strat_train_x = strat_train_set.drop("median_house_value", axis=1) # drop labels for training set
housing_labels = strat_train_set["median_house_value"].copy()
sample_incomplete_rows = strat_train_x[strat_train_x.isnull().any(axis=1)]
sample_incomplete_rows.head()
# Check number of rows that contains NaN
len(sample_incomplete_rows)
imputer = SimpleImputer(strategy="median")
#housing_num = strat_train_x.drop("ocean_proximity", axis=1)
# alternatively: 
housing_num = strat_train_x.select_dtypes(include=[np.number])
imputer.fit(housing_num)
housing_cat = housing[["ocean_proximity"]]

ordinal_encoder = OrdinalEncoder()
housing_cat_encoded = ordinal_encoder.fit_transform(housing_cat)


#ordinal_encoder.categories_

cat_encoder = OneHotEncoder()
housing_cat_1hot = cat_encoder.fit_transform(housing_cat)

#housing_cat_1hot.toarray()
cat_encoder = OneHotEncoder(sparse=False)
housing_cat_1hot = cat_encoder.fit_transform(housing_cat)


#cat_encoder.categories_

# column index
rooms_ix, bedrooms_ix, population_ix, households_ix = 3, 4, 5, 6

class CombinedAttributesAdder(BaseEstimator, TransformerMixin):
    def __init__(self, add_bedrooms_per_room = True): # no *args or **kargs
        self.add_bedrooms_per_room = add_bedrooms_per_room
    def fit(self, X, y=None):
        return self  # nothing else to do
    def transform(self, X):
        rooms_per_household = X[:, rooms_ix] / X[:, households_ix]
        population_per_household = X[:, population_ix] / X[:, households_ix]
        if self.add_bedrooms_per_room:
            bedrooms_per_room = X[:, bedrooms_ix] / X[:, rooms_ix]
            return np.c_[X, rooms_per_household, population_per_household,
                         bedrooms_per_room]
        else:
            return np.c_[X, rooms_per_household, population_per_household]

attr_adder = CombinedAttributesAdder(add_bedrooms_per_room=False)
housing_extra_attribs = attr_adder.transform(housing.values)

imputer = SimpleImputer(strategy="median")

num_pipeline = Pipeline([
        ('imputer', SimpleImputer(strategy="median")),
        ('attribs_adder', CombinedAttributesAdder()),
        ('std_scaler', StandardScaler()),
    ])

housing_num_tr = num_pipeline.fit_transform(housing_num)
#housing_num_tr

num_attribs = list(housing_num)
cat_attribs = ["ocean_proximity"]

full_pipeline = ColumnTransformer([
        ("num", num_pipeline, num_attribs),
        ("cat", OneHotEncoder(), cat_attribs),
    ])

housing_prepared = full_pipeline.fit_transform(strat_train_x)
housing_prepared
housing_prepared.shape

try:
    pickle.dump(housing, open(f"{PICKLE_PATH}/housing.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(strat_train_set, open(f"{PICKLE_PATH}/strat_train_set.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(strat_test_set, open(f"{PICKLE_PATH}/strat_test_set.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(strat_train_x, open(f"{PICKLE_PATH}/strat_train_x.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(housing_labels, open(f"{PICKLE_PATH}/housing_labels.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(housing_num, open(f"{PICKLE_PATH}/housing_num.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(housing_cat, open(f"{PICKLE_PATH}/housing_cat.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(housing_prepared, open(f"{PICKLE_PATH}/housing_prepared.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

